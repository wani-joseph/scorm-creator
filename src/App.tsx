import { FC } from "react";
import Slides from "./components/Slides";

const App: FC = () => <Slides />;

export default App;
