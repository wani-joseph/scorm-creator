import { render, cleanup } from "@testing-library/react";

import Confirmation from "./index";

describe("Navigation Component", () => {
  it("should render error message", () => {
    const wrapper = render(
      <Confirmation error={true} message={"This is error message"} />
    );
    expect(wrapper.getByTestId("error-message")).toBeInTheDocument();
    expect(wrapper.getByTestId("error-icon")).toBeInTheDocument();
  });

  it("should render success message", () => {
    const wrapper = render(
      <Confirmation error={false} message={"This is success message"} />
    );
    expect(wrapper.getByTestId("success-message")).toBeInTheDocument();
    expect(wrapper.getByTestId("success-icon")).toBeInTheDocument();
  });

  afterAll(cleanup);
});
