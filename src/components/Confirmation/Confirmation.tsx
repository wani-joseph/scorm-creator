import { FC } from "react";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";

import CloseIcon from "@mui/icons-material/Close";
import CheckIcon from "@mui/icons-material/Check";

interface Props {
  error: boolean | null;
  message: string;
}

const Confirmation: FC<Props> = ({ error, message }) => (
  <>
    {error === true && (
      <Box>
        <CloseIcon data-testid="error-icon" />
        <Typography data-testid="error-message">{message}</Typography>
      </Box>
    )}
    {error === false && message.length > 0 && (
      <Box>
        <CheckIcon data-testid="success-icon" />
        <Typography data-testid="success-message">{message}</Typography>
      </Box>
    )}
  </>
);

export default Confirmation;
