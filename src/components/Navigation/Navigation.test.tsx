import { render, cleanup } from "@testing-library/react";

import Navigation from "./index";
import { SlideTypes } from "../../models/constants";

describe("Navigation Component", () => {
  it("question slide type should render Next Prev buttons disabled", () => {
    const wrapper = render(
      <Navigation
        hasError={false}
        disableNext={true}
        disablePrev={true}
        slideType={SlideTypes.TEXT}
      />
    );
    expect(wrapper.getByTestId("next-button")).toHaveProperty("disabled", true);
    expect(wrapper.getByTestId("prev-button")).toHaveProperty("disabled", true);
  });

  it("text slide type should render Next Prev buttons not disabled", () => {
    const wrapper = render(
      <Navigation
        hasError={false}
        disableNext={false}
        disablePrev={false}
        slideType={SlideTypes.TEXT}
      />
    );
    expect(wrapper.getByTestId("next-button")).toHaveProperty(
      "disabled",
      false
    );
    expect(wrapper.getByTestId("prev-button")).toHaveProperty(
      "disabled",
      false
    );
  });

  it("question slide type should render Prev and Send button", () => {
    const wrapper = render(
      <Navigation
        hasError={null}
        disableNext={false}
        disablePrev={false}
        slideType={SlideTypes.QUESTION}
      />
    );
    expect(wrapper.getByTestId("send-button")).toHaveProperty(
      "disabled",
      false
    );
    expect(wrapper.getByTestId("prev-button")).toHaveProperty(
      "disabled",
      false
    );
  });

  it("question slide type should render Prev and And Try Again Button", () => {
    const wrapper = render(
      <Navigation
        hasError={true}
        disableNext={false}
        disablePrev={false}
        slideType={SlideTypes.QUESTION}
      />
    );
    expect(wrapper.getByTestId("tryagain-button")).toHaveProperty(
      "disabled",
      false
    );
    expect(wrapper.getByTestId("prev-button")).toHaveProperty(
      "disabled",
      false
    );
  });

  it("question slide type should render Prev and Next Button", () => {
    const wrapper = render(
      <Navigation
        hasError={false}
        disableNext={false}
        disablePrev={false}
        slideType={SlideTypes.QUESTION}
      />
    );
    expect(wrapper.getByTestId("next-button")).toHaveProperty(
      "disabled",
      false
    );
    expect(wrapper.getByTestId("prev-button")).toHaveProperty(
      "disabled",
      false
    );
  });

  afterAll(cleanup);
});
