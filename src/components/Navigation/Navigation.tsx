import { FC } from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";

import Confirmation from "../Confirmation";
import MuiButton from "../UI/MuiButton";
import { SlideTypes } from "../../models/constants";

interface Props {
  hasError: boolean | null;
  disableNext: boolean;
  disablePrev: boolean;
  slideType: string;
}

const Navigation: FC<Props> = ({
  disableNext,
  disablePrev,
  slideType,
  hasError,
}) => {
  const handleNextButton = () => {
    if (slideType === SlideTypes.TEXT)
      return (
        <MuiButton
          testId={"next-button"}
          disabled={disableNext}
          label={"Next"}
        />
      );

    if (slideType === SlideTypes.QUESTION && hasError === null) {
      return (
        <MuiButton testId={"send-button"} disabled={false} label={"Send"} />
      );
    }

    if (slideType === SlideTypes.QUESTION && hasError === false) {
      return (
        <MuiButton
          testId={"next-button"}
          disabled={disableNext}
          label={"Next"}
        />
      );
    }

    if (slideType === SlideTypes.QUESTION && hasError === true) {
      return (
        <>
          <MuiButton
            testId={"tryagain-button"}
            disabled={false}
            label={"Try Again"}
          />
          <MuiButton
            testId={"next-button"}
            disabled={disableNext}
            label={"Next"}
          />
        </>
      );
    }
  };

  return (
    <>
      <Confirmation error={hasError} message="This is not working..." />
      <Box>{handleNextButton()}</Box>
      <Button data-testid="prev-button" disabled={disablePrev}>
        Prev
      </Button>
    </>
  );
};

export default Navigation;
