import { FC } from "react";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";

import Navigation from "../Navigation";

import { SlideTypes } from "../../models/constants";

const Slides: FC = () => (
  <Container maxWidth="md">
    <Typography>Slides</Typography>
    <Navigation
      hasError={true}
      disableNext={true}
      disablePrev={true}
      slideType={SlideTypes.TEXT}
    />
  </Container>
);

export default Slides;
