import { FC } from "react";
import Button from "@mui/material/Button";

interface Props {
  disabled: boolean;
  testId: string;
  label: string;
}

const MuiButton: FC<Props> = ({ testId, disabled, label }) => (
  <Button data-testid={testId} disabled={disabled}>
    {label}
  </Button>
);

export default MuiButton;
